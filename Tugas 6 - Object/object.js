// No 1
function arrayToObject(arr) {
    var now = new Date();
    var year = now.getFullYear();
    for(let i = 0; i<arr.length; i++){
        var robj = {
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2]
        };
        if(arr[i].length !== 4 || arr[i][3]>year){
            robj.age = "Invalid Birth Year";
        }else{
            robj.age = year - arr[i][3];
        }
        var string = JSON.stringify(robj);
        console.log(`${i+1}. ${robj.firstName} ${robj.lastName}: ${string}`);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/ 
// Error case 
arrayToObject([]) // ""

// No 2
function shoppingTime(memberId, money) {
}
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// No 3
function naikAngkot(arrPenumpang) {
    var result = [];
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    for(let j = 0; j<arrPenumpang.length; j++){
        var robj = {
        penumpang: arrPenumpang[j][0],
        naikDari: arrPenumpang[j][1],
        tujuan: arrPenumpang[j][2],
        bayar: (rute.indexOf(arrPenumpang[j][2])-rute.indexOf(arrPenumpang[j][1]))*2000
    }
    result.push(robj);
}
    return result;
}
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]
